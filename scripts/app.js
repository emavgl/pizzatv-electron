(function () {
    'use strict';

    var app = angular.module(
        'app',
        [
            'ngRoute',
            'ngMaterial',
            'ngAnimate',
            'toastr',
            'LocalStorageModule',
            'ngSanitize',
            'com.2fdevs.videogular',
            'com.2fdevs.videogular.plugins.controls',
            'com.javiercejudo.videogular.plugins.autohide-cursor',
            'duScroll'
        ]
    );

    app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setStorageType('sessionStorage');
    });

    app.config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);

    app.config(['$routeProvider',
                function($routeProvider) {
                    $routeProvider.
                    when('/', {
                        templateUrl: './scripts/views/search.html',
                        controller: 'searchCtrl'
                    }).
                    when('/tv', {
                        templateUrl: './scripts/views/search.html',
                        controller: 'searchCtrl'
                    }).
                    when('/detail', {
                        templateUrl: './scripts/views/detail.html',
                        controller: 'detailCtrl'
                    }).
                    when('/favorite', {
                        templateUrl: './scripts/views/favorite.html',
                        controller: 'favoriteCtrl'
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
                }]);

    app.controller('appCtrl', function($scope, $rootScope, $location, localStorageService) {
        localStorageService.set('target', 'movie', 'sessionStorage');

        $scope.canGoBack = function(){
            history.back();
            scope.$apply();
        }

        var target = 'movie';
        $scope.currentLabel = 'movie';

        $scope.navigateTo = function(path){
            target = 'movie';
            if (path == '/tv') target = 'tv';
            if ($location.path() == path) return;

            localStorageService.set('target', target, 'sessionStorage');
            
            // reset rootscope
            $rootScope.films = [];
            $rootScope.searchInfo = {};

            $location.path(path).replace();

            console.log("cambiato in " + target)
        };
    });
})();
