angular.module('app').controller('searchCtrl', function($scope, $rootScope, $location, $http, $location, localStorageService, $rootScope, itaService, pizzaService) {
        // Get the target
        var target = localStorageService.get('target', 'sessionStorage');
        $scope.target = target;

        // Get last show on history
        var lastFromHistory = pizzaService.getShowFromHistory(target);
        $scope.isReccomandationAvailable = (lastFromHistory != undefined);

        // Download downloaded toplist for the selected target
        var downloaded = localStorageService.get('toplist' + target, 'sessionStorage');

        // Search user input        
        var search = function(title){
            $scope.loading = true;
            foundPromise = itaService.search(title, target);
            foundPromise.then(function(entries){
                (entries.data).forEach(function(entry) {
                    // Convert name in title for tv series
                    if (!entry.title) entry.title = entry.name;
                }, this);
                $rootScope.films = entries.data;
                $scope.loading = false;
            })
        }

        $scope.downloadTopList = function(showToplist){
            $scope.loading = true;
            if (showToplist){
                    // Show toplist
                    console.log("getting toplist " + target);
                    topPromise = itaService.getTopList(target, false);
            } else {
                    // reccomanded
                    console.log("getting reccomanded " + target, lastFromHistory);
                    topPromise = itaService.getTopList(target, lastFromHistory);
            }
            topPromise.then(function(entries){
                (entries.data).forEach(function(entry) {
                    if (!entry.title) entry.title = entry.name;
                }, this);
                console.log("after promis", entries.data);
                $rootScope.films = entries.data;
                localStorageService.set('toplist' + target, $rootScope.films, 'sessionStorage');
                $scope.loading = false;
             }).catch(function(err){
                 console.log("error", err);
             });
        }

        // If is the first time in this page
        // Download the toplist for the target and store it
        console.log("downloaded is", downloaded)
        if (!downloaded){
            console.log("download again");
            $scope.downloadTopList(false);
        } else {
            console.log("reuse list");
            if ($rootScope.films.length == 0) $rootScope.films = downloaded;
        }

        // Trigger the search functions
        $scope.buttonClick = function() {
            search($rootScope.searchInfo.title);
        };

        // Called when you click an item of the grid
        // Should pass the item using localStorageService to
        // the detail page.
        $scope.goto = function(item){
            localStorageService.set('detail', item, 'sessionStorage');
            $location.path('/detail');
        };
});