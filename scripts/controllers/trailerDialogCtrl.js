angular.module('app').controller('trailerDialogCtrl', function($scope,  $mdDialog, items, $sce) {
    $scope.videos = items;
    $scope.playUrl = $sce.trustAsResourceUrl(items[0].url + '?modestbranding=1&iv_load_policy=3&rel=0');

    $scope.closeDialog = function() {
        $mdDialog.hide();
    }

    $scope.changeVideo = function(videoItem){
        videoItem.url += '?modestbranding=1&iv_load_policy=3&rel=0'
        $scope.playUrl = $sce.trustAsResourceUrl(videoItem.url);
    };
});