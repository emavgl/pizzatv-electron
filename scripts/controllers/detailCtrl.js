var vlc = require('./modules/vlc.js');
var parseUtil = require('./modules/parseurl.js');
var ncp = require("copy-paste");
const os = require('os');
const dwn = require('./modules/download.js');
var opn = require('opn');
var url = require('url');
var request = require('request');

angular.module('app').controller('detailCtrl', function($scope, $http, $mdToast, $mdDialog, toastr, localStorageService, itaService, $sce, pizzaService, $rootScope, $document, $mdMedia) {
        $scope.searchInfo = {title: ''};
        $scope.loading = false;

        var nextEpisodeToast;

        $rootScope.$mdMedia = $mdMedia;

        // Get the target
        var target = localStorageService.get('target', 'sessionStorage');
        $scope.target = target;
        
        var getNextEpisode = async function(showId, season_number, episode_number) {
            // Get season info
            let episodeSeason = await itaService.getSeasonInfo(showId, season_number);
            episodeSeason = episodeSeason.data.seasonInfo;
            // If season contains other episode, return next episode in the list
            if (episodeSeason.episodes.length > episode_number) {
                $scope.episodes = episodeSeason.episodes;
                return episodeSeason.episodes[episode_number];
            }
            else {
              // Episode is the last episode of the season
              // Get next season number
              let seasonNumber = episodeSeason.season_number;
              let nextSeason = $scope.detail.seasons.find(x  => x.season_number == seasonNumber + 1);
              if (nextSeason) {
                // if exists a new season, get episodes of new season
                episodeSeason = await itaService.getSeasonInfo(this.show.id, seasonNumber + 1);
                episodeSeason = episodeSeason.data.seasonInfo;
                $scope.episodes = episodeSeason;
                // return first episode of the new season
                return episodeSeason.episodes[0];
              } else {
                return null; // no next episode found
              }
            }
        }

        // Search videos, put the list into $scope.results
        $scope.search = async function(){
			$scope.loading = true;

            try {

                if (target == 'tv') {
                    toastr.info('Sto cercando episodio: ' + $scope.searchInfo.season_number + 'x' + $scope.searchInfo.episode_number);
                    console.log("CERCANDO", "TV", $scope.searchInfo.showId, $scope.searchInfo.season_number, $scope.searchInfo.episode_number );
                    dataResponse = await itaService.getTvShow($scope.searchInfo.showId, $scope.searchInfo.season_number, $scope.searchInfo.episode_number);
                } else {
                    toastr.info('Sto cercando il film: ' + $scope.searchInfo.title);
                    console.log("CERCANDO", "MOVIE", $scope.searchInfo.showId, $scope.searchInfo.showId, $scope.searchInfo.title);
                    dataResponse = await itaService.getMovies($scope.searchInfo.showId, $scope.searchInfo.title);
                }

                if (nextEpisodeToast) {
                    $mdToast.hide(nextEpisodeToast);
                }

                let results = dataResponse.data.results;
                let videos = [];
                for (const result of results) {
                    let pageTitle = result.show.title;
                    let pageVideos = result.videos;
                    pageVideos.forEach(video => video.title = pageTitle); 
                    videos = videos.concat(pageVideos);
                }
                $scope.videos = videos;

                // Show message
                if ($scope.videos.length > 0) {
                    // soft scrolling here
                    var listVideos = angular.element(document.getElementById('listVideos'));
                    if (!$scope.isDialogOpen) $document.scrollToElementAnimated(listVideos, 30, 2000);
                    toastr.info('Lista scaricata');
                } else {
                    toastr.warning('Nessun risultato trovato');
                    $scope.changeTitleDialog();
                }
                $scope.loading = false;                
            } catch (error) {
                //error handling logic
                $scope.loading = false;
                if (error.name != "TypeError")  toastr.error('Ops. Problema nel recuperare i dati');
                console.error(error);
            }
        
        }

        $scope.searchEpisode = function(episode_season, episode_number){
            let episode = $scope.episodes.find(e => e.episode_number == episode_number);
            $scope.searchInfo.episode_number = episode_number;
            $scope.searchInfo.season_number = episode_season;
            if (episode.still_path) $scope.detail.backdrop_path = episode.still_path;
            $scope.search();
        }

        $scope.showTrailerDialog = function($event) {
            var parentEl = angular.element(document.body);
            $scope.isDialogOpen = true;
            $mdDialog.show({
                parent: parentEl,
                targetEvent: $event,
                templateUrl: 'scripts/views/trailerDialog.html',
                locals: {
                    items: $scope.detail.videos
                },
                controller: 'trailerDialogCtrl',
                clickOutsideToClose: true,
                onRemoving: function (event) {
                    $scope.isDialogOpen = false;
                }
            });
        }

        // Get passed element from search page page
        var passedItem = localStorageService.get('detail', 'sessionStorage');
        if (passedItem) {
            $scope.detail = passedItem;
            console.log("passeditem", passedItem);
            showId = passedItem.id;

            infoResult = itaService.getInfo(passedItem.title, target, showId);

            infoResult.then(function(entries){
                if (entries.data.infoLabels) {
                    $scope.detail = entries.data.infoLabels;
                    if (target == 'tv') {
                        $scope.detail.title = $scope.detail.name;
                    }
                }
            });

            // Insert it as history to improve toplist with recommendations
            pizzaService.insertShowOnHistory(target, showId);

            $scope.favorite = pizzaService.searchFavorite(showId, target);

            // Get the link and start looking for videos
            cleaned = passedItem.title.toLowerCase().trim();
            $scope.searchInfo.title = cleaned;
            $scope.searchInfo.showId = showId;

            // start searching for a movie
            if (target == 'movie') $scope.search();
            if (target == 'tv') {
                let lastEpisode = pizzaService.getLastEpisode(showId, "tv");
                if (lastEpisode) {
                    $scope.lastSeenEpisode = lastEpisode;
                    let parts = lastEpisode.split("x");
                    getNextEpisode(showId, parts[0], parts[1]).then((nextEpisode) => {
                        if (nextEpisode) {
                            let nextEpisodeStr = nextEpisode.season_number + "x" + nextEpisode.episode_number;
                            nextEpisodeToast = showNextEpisodeToast("Prossimo episodio: " + nextEpisodeStr)
                            nextEpisodeToast.then(function(response) {
                                if (response === "ok") {
                                    $scope.searchEpisode(nextEpisode.season_number, nextEpisode.episode_number);
                                }
                            });
                        } else {
                            console.log("Nessun episodio trovato");
                        }
                    });
                }
            }
            
            // removed item from temp storage
            localStorageService.remove('detail', 'sessionStorage');
        }

        var parseUrl = function(item){
            toastr.info('Estraendo...');
            let destUrl = item.url;
            promiseUrl = parseUtil.getParsedUrl(destUrl);
            promiseUrl.then(parsedUrl => {
                console.log("parsed-url", parsedUrl);
                toastr.info('Fatto!');
                item.playUrl = parsedUrl;
                $scope.loading = false;   
                item.loading = false;

                if ($scope.target == 'tv') {
                    let seasonAndEpisode = $scope.searchInfo.season_number + "x" + $scope.searchInfo.episode_number;
                    pizzaService.saveEpisode($scope.searchInfo.showId, 'tv', seasonAndEpisode);
                    console.log("saved ", $scope.searchInfo.showId, seasonAndEpisode);
                }

              }).catch(error => {
                item.expired = true;
                toastr.error('Non funzionante');
                $scope.loading = false;
                item.loading = false;
            });
        }

        // Get list of episodes once user've selected the season
        $scope.getListEpisodesSeason = function(){
            // Get season info
            var season_number = $scope.searchInfo.season_number;
            var seasonInfoPromise = itaService.getSeasonInfo(showId, season_number);
            $scope.loading = true;
            seasonInfoPromise.then(function(entries){
                console.log("season info");
                $scope.loading = false;
                console.log(entries.data.seasonInfo.episodes);
                $scope.episodes = entries.data.seasonInfo.episodes;
            });
        }

        // Called from html when I click over an element
        // of videos list
        $scope.checkUrl = function(item){
            $scope.selectedItem = item;
            parseUrl(item);
        };

        // Play video with videogular
        $scope.playVideo = function(directVideoUrl){
            $scope.playItem = {};
            $scope.playItem.config = {
                sources: [
                    {src: $sce.trustAsResourceUrl(directVideoUrl), type: "video/mp4"}
                ],
                plugins: {
                    controls: {
                        autoHide: true,
                        autoHideTime: 10000
                    }
                },
                theme: "node_modules/videogular-themes-default/videogular.css"
            };
            toastr.success('Il video comincia tra qualche secondo');
            window.scrollTo(0, 0);
        }

        // Copy on clipboard actions
        $scope.copyOnClipboard = function(text){
            ncp.copy(text, function () {
                    toastr.success('Copiato!');
            });
        }

        // Download action
        // Use uGet otherwise open on browser
        $scope.download = function(videoUrl){
            var filename = $scope.searchInfo.title;

            if (target == 'tv'){
                filename += "-";
                filename += "S" + $scope.searchInfo.season_number;
                filename += "E" + $scope.searchInfo.episode_number;
            }

            filename = filename + ".mp4";

            dwn.downloadUrl(videoUrl, filename);
            toastr.success('In download!');
        }

        $scope.openInBrowser = function(urlToOpen){
            // Opens the url in the default browser
            opn(urlToOpen);
        }

        // Play on VLC
        $scope.playVLC = function(directVideoUrl){
            if (os.platform() == 'win32'){
                result = vlc.playWindows(directVideoUrl);
                if(!result) toastr.error('VLC non trovato');
            } else {				
                try {
                    opn(directVideoUrl, {app: ['vlc', '--fullscreen']});
                }
                catch(err) {
                    toastr.error('VLC non trovato');
                }
            }
        };

        // Get host name from url
        $scope.extractHostName = function(urlToParse) {
            return url.parse(urlToParse).hostname;
        };

        // Helper function
        $scope.isNull = function(item){
            if(!item) return true;
            return false;
        };

        // Method called when the user click on the star
        $scope.changeFavorite = function(){
            if ($scope.favorite){
                // Show is already on favorites
                // Remove it
                pizzaService.deleteFavorite(showId, target);
                $scope.favorite = false;
                toastr.info('Rimosso dai preferiti');
            } else {
                // Add show
                pizzaService.insertFavorite(showId, target, $scope.detail.title, $scope.detail.poster_path);
                $scope.favorite = true;
                toastr.info('Aggiunto ai preferiti');
            }
        }

        $scope.isSmallScreen = function(){
            return $mdMedia('xs');
        };

        $scope.isMac = function(){
            return os.platform() == 'darwin';
        };

        $scope.showAlert = function(ev, message) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Trama')
                    .textContent(message)
                    .ariaLabel('Dettagli trama')
                    .ok('Chiudi')
                    .targetEvent(ev)
                );
        };

        $scope.changeTitleDialog = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var message = 'Non sono riusciuto a trovare niente usando "' + $scope.searchInfo.title + '"';
            message += ". Vuoi provare a avviare una ricerca con un altro titolo? Inserisci il titolo sotto.";
            var confirm = $mdDialog.prompt()
            .title('Vorresti cercare con un altro titolo?')
            .textContent(message)
            .placeholder('Titolo')
            .ariaLabel('Titolo')
            .initialValue($scope.searchInfo.title)
            .targetEvent(ev)
            .ok('Cerca')
            .cancel('Cancella');

            $mdDialog.show(confirm).then(function(result) {
                $scope.searchInfo.title = result;
                $scope.search();
            }, function() {
                
            }); 
        };

        $scope.openManualSelectDialog = function(ev) {
            var msg = "Ricerca manuale. Immetti {stagione}-{episodio}";
            var exampleMsg = "Esempio. Se vuoi guardare stagione 3, episodio 15, inserisci: 3-15";
            var confirm = $mdDialog.prompt()
            .title(msg)
            .textContent(exampleMsg)
            .placeholder('Episodio')
            .ariaLabel('Episodio')
            .initialValue("1-1")
            .targetEvent(ev)
            .ok('Cerca')
            .cancel('Cancella');

            $mdDialog.show(confirm).then(function(result) {
                var parts = result.split('-');
                $scope.searchInfo.season = parts[0];
                $scope.searchInfo.episode = parts[1];
                $scope.search();
            }, function() {
                
            });
        }

        var showNextEpisodeToast = function(text) {
            return $mdToast.show({
                hideDelay: 0,
                position: 'bottom left',
                controller: function($scope, $mdToast, toastMessage){
                    $scope.toastMessage = toastMessage;
                    $scope.closeToast = () => $mdToast.hide("undo");
                    $scope.startSearch = () => $mdToast.hide("ok");
                },
                locals: {toastMessage: text},
                templateUrl: "scripts/views/next-episode-toast.html"
              });
        }
});
