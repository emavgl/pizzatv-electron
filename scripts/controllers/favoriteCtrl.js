angular.module('app').controller('favoriteCtrl', function($scope, $location, localStorageService, $rootScope, pizzaService) {
    $scope.favorites = pizzaService.getFavorites();

    // Called when you click an item of the grid
    // Should pass the item using localStorageService to
    // the detail page.
    $scope.goto = function(item){
        localStorageService.set('detail', item, 'sessionStorage');
        localStorageService.set('target', item.target, 'sessionStorage');
        $location.path('/detail');
    };
});