var app = angular.module('app');

app.factory('pizzaService', function($http, localStorageService) {

    var getFavorites = function(){
        var favorites = localStorageService.get("favorites", "localStorage");
        if (favorites == undefined){
            // No list initialized yet
            console.log("initilize list");
            localStorageService.set("favorites", [], "localStorage");
            favorites = [];
        }
        return favorites;
    }

    var insertFavorite = function(showId, target, title, poster_path){
        var newFavorite = {"id": showId, "target": target, "title": title, "poster_path": poster_path};
        var favorites = getFavorites();
        favorites.push(newFavorite);
        localStorageService.set("favorites", favorites, "localStorage");
    }

    var deleteFavorite = function(showId, target){
        var favorites = getFavorites();
        console.log(showId, target);
        var filtered = favorites.filter(x => !(x.id == showId && x.target == target));
        localStorageService.set("favorites", filtered, "localStorage");
    }

    var searchFavorite = function(showId, target){
        var favorites = getFavorites();
        var filtered = favorites.filter(x => x.id == showId && x.target == target);
        if (filtered.length > 0) return true;
        return false;
    }

    var insertShowOnHistory = function(target, showId){
        localStorageService.set(target + "last", showId, "localStorage");
    }

    var getShowFromHistory = function(target){
        return localStorageService.get(target + "last", "localStorage");
    }

    var getLastEpisode = function(showId, target) {
        let key = showId + target;
        return localStorageService.get(key, "localStorage");
    }

    var saveEpisode = function(showId, target, episode) {
        let key = showId + target;
        localStorageService.set(key, episode, "localStorage");
    }

    return {
    'insertShowOnHistory': insertShowOnHistory,
    'getShowFromHistory': getShowFromHistory,
    'getFavorites': getFavorites,
    'insertFavorite': insertFavorite,
    'deleteFavorite': deleteFavorite,
    'searchFavorite': searchFavorite,
    'getLastEpisode': getLastEpisode,
    'saveEpisode': saveEpisode
    }
});