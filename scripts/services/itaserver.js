var app = angular.module('app');
const util = require('util');

app.factory('itaService', function($http) {
    var base_url = "https://pizzatv.evgl.ovh";
    //var base_url = "http://localhost:4567"
    
    var getTopList = function(target, showId){
        var params = {};
        params.target = target;
        var req_url = base_url + "/toplist";
        if (showId) params.showId = showId;
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    }

    var getInfo = function(title, target, showId){
        var req_url = base_url + "/info";
        title = title.replace(" ", "+");
        var params = {};
        params.title = title;
        params.target = target;
        if (showId) params.showId = showId;
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    }

    var getSeasonInfo = function(showId, season){
        var req_url = base_url + "/seasoninfo";
        var params = {};
        params.showId = showId;
        params.season = season;
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    }

    var search = function(title, target){
        var params = {};
        params.title = title;
        params.target = target;
        var req_url = base_url + "/search";
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    }

    var getTvShow = function(showId, season, episode){
        var params = {};
        params.showId = showId;
        params.season = season;
        params.episode = episode;
        var req_url = base_url + "/tv";
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    }

    var getMovies = function(showId, title){
        title = title.replace(" ", "+");
        var params = {};
        params.showId = showId;
        params.title = title;
        var req_url = base_url + "/movie";
        return $http({
            method: 'GET',
            url: req_url,
            params: params
        });
    };

    return { 'getSeasonInfo': getSeasonInfo,'getTopList': getTopList, 'getTvShow': getTvShow,'getInfo': getInfo, 'getMovies': getMovies, 'search': search};
});
