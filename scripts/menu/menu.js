(function () {
    'use strict';

    // Module automatically included (only) in the Renderer process (Electron)
    //noinspection NodeRequireContents
    var app = require('electron').app;
    var remote = require('electron').remote;
    var BrowserWindow = require('electron').BrowserWindow;  // Module to create native browser window.
    var dwn = require('./modules/download.js');
    var Menu = remote.Menu;

    var template = [
        {
            label: 'Indietro',
            click: function(item, focusedWindow) {
                        focusedWindow.webContents.goBack();
            }
        },
        {
            label: 'App',
            submenu: [
                {
                    label: 'Esci',
                    accelerator: 'CmdOrCtrl+Q',
                    role: 'close'
                }
            ]
        },
        {
            label: 'Dev',
            submenu: [
                {
                    label: 'Reload',
                    accelerator: 'CmdOrCtrl+R',
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.reload();
                    }
                },
                {
                    label: 'Toggle Full Screen',
                    accelerator: (function() {
                        if (process.platform == 'darwin')
                            return 'Ctrl+Command+F';
                        else
                            return 'F11';
                    })(),
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.setFullScreen(!focusedWindow.isFullScreen());
                    }
                },
                {
                    label: 'Toggle Developer Tools',
                    accelerator: (function() {
                        if (process.platform == 'darwin')
                            return 'Alt+Command+I';
                        else
                            return 'Ctrl+Shift+I';
                    })(),
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.toggleDevTools();
                    }
                }
            ]
        }
    ];

    var menuUgetTemplate = [
        {
            label: 'Indietro',
            click: function(item, focusedWindow) {
                        focusedWindow.webContents.goBack();
            }
        },
        {
            label: 'App',
            submenu: [
                {
                    label: 'Apri Download Manager (uGet)',
                    click: function(item, focusedWindow) {
                        dwn.openDownloadManager();
                    }
                },
                {
                    label: 'Esci',
                    accelerator: 'CmdOrCtrl+Q',
                    role: 'close'
                }
            ]
        },
        {
            label: 'Dev',
            submenu: [
                {
                    label: 'Reload',
                    accelerator: 'CmdOrCtrl+R',
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.reload();
                    }
                },
                {
                    label: 'GoBack',
                    click: function(item, focusedWindow) {
                        focusedWindow.webContents.goBack();
                    }
                },
                {
                    label: 'Toggle Full Screen',
                    accelerator: (function() {
                        if (process.platform == 'darwin')
                            return 'Ctrl+Command+F';
                        else
                            return 'F11';
                    })(),
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.setFullScreen(!focusedWindow.isFullScreen());
                    }
                },
                {
                    label: 'Toggle Developer Tools',
                    accelerator: (function() {
                        if (process.platform == 'darwin')
                            return 'Alt+Command+I';
                        else
                            return 'Ctrl+Shift+I';
                    })(),
                    click: function(item, focusedWindow) {
                        if (focusedWindow)
                            focusedWindow.toggleDevTools();
                    }
                }
            ]
        }
    ];

    var menuUget = Menu.buildFromTemplate(menuUgetTemplate);
    var menu = Menu.buildFromTemplate(template);


    if (process.platform == 'win32' || process.platform == 'linux'){
        Menu.setApplicationMenu(menuUget);
    } else {
        Menu.setApplicationMenu(menu);
    }

    //Menu.setApplicationMenu(null);

})();
