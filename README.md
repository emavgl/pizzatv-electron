# PizzaTV

## Project

- css
- img
- modules (imported modules)
- node_modules (node modules)
- scripts (angular project directory)
--- controllers
--- views
--- services
--- directives
--- menu ( handles electron menu )
--- app.js (entry point, routing)

## Requirements

- Node latest version (7+)
- Electron (npm install -g electron)

## TODO:
- add a page for favorite shows (SQLite Database is needed, save only title, poster_path, id)