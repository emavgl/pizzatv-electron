var wstream = require('./servers/wstream.js');
var speedvideo = require('./servers/speedvideo.js');
var mixdrop = require('./servers/mixdrop.js');
var akvideo = require('./servers/akvideo.js');
var vcrypt = require('./servers/vcrypt.js');

var url = require('url');

function shouldDownloadDocument(toParseUrl){
    let hostname = url.parse(toParseUrl).hostname;
    return hostname.includes("openload") || hostname.includes("oload") || hostname.includes("streamango");
}

async function getParsedUrl(toParseUrl, doc) {
    var hostname = url.parse(toParseUrl).hostname;
    var services = [
                    {lib: wstream, name: "wstream"},
                    {lib: speedvideo, name: "speedvideo"},
                    {lib: mixdrop, name: "mixdrop"},
                    {lib: akvideo, name: "akvideo"}
                ];
    
    if (hostname.includes('vcrypt')) {
        toParseUrl = await vcrypt.getDestinationUrl(toParseUrl);
        console.log("Vcrypt url bypassed", toParseUrl);
        hostname = url.parse(toParseUrl).hostname;
    }

    // parse the url
    for (service of services) {
        if (hostname.includes(service.name)){
            return await service.lib.getParsedUrl(toParseUrl);
        }
    }

    return toParseUrl;
};

module.exports.getParsedUrl = getParsedUrl;
module.exports.shouldDownloadDocument = shouldDownloadDocument;
