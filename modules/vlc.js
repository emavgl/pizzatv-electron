const fs = require('fs');
const os = require('os');
const spawn = require('child_process').spawn;

var playWindows = function(url){
	if (os.platform() != 'win32') return null;
	var p1 = 'C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe';
	var p2 = 'C:\\Program Files\\VideoLAN\\VLC\\vlc.exe';
	
	path = "";
	if (fs.existsSync(p1)) {
		path = p1;
	} else if (fs.existsSync(p2)) {
		path = p2;
	} else {
		return null;
	}
	
	arguments = [url, "--fullscreen"]
	var play = spawn(path, arguments, {detached: true});
	return true;
}

module.exports.playWindows = playWindows;
