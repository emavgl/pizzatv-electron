var findMultipleMatch = function(data, reg){
    reg = RegExp(reg, 'g')
    var results = [];
    while ((match = reg.exec(data)) !== null) {
        results.push(match);
    }
    return results;
}

module.exports.findMultipleMatch = findMultipleMatch;