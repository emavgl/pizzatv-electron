const fs = require('fs');
const os = require('os');
const spawn = require('child_process').spawn;
var opn = require('opn');

var downloadUrl = function(url, filename){
	let platform = os.platform();
	let homeDirectory = os.homedir();
	var ugetPath = "./uget/bin/uget.exe";
	var args = ["--quiet"];
	switch(platform) {
	    case 'win32':
			let downloadPath = homeDirectory + "\\Downloads\\";
//			args.push("--folder=" + downloadPath)
			if (filename) args.push("--filename=" + filename)
			args.push(url);
			var play = spawn(ugetPath, args, {detached: true});
	        break;
		case 'linux':
			ugetPath = "./uget/bin/uget";
			args = ["--quiet"]
			if (filename) args.push("--filename=" + filename)
			args.push(url);
			var play = spawn(ugetPath, args, {detached: true});
			break;
	    default:
	        opn(url);
	} 
	return true;
}



var openDownloadManager = function(url, filename){
	let platform = os.platform();
	var ugetPath = "./uget/bin/uget.exe";
	if (platform == "linux")
		ugetPath = "./uget/bin/uget";
	let args = []
	var play = spawn(ugetPath, args, {detached: true});
	return true;
}


module.exports.downloadUrl = downloadUrl;
module.exports.openDownloadManager = openDownloadManager;



