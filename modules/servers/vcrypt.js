var rp = require('request-promise-native');
var unpack = require('../unpacker.js');
var scraperutil = require('../scraperutil.js');

var exports = module.exports = {};

async function getDestinationUrl(vcryptUrl) {
    let redirectedUrl = await getRedirectedUrl(vcryptUrl);
    if (redirectedUrl) {
        if (redirectedUrl.includes("4snip")) {
            return bypass4snip(redirectedUrl);
        } else {
            return redirectedUrl;
        }
    } else {
        return null;
    }
}

async function bypass4snip(snipUrl) {
    let pathParameters = snipUrl.split("/")
    let postUrl = "https://4snip.pw/outlink/" + pathParameters[pathParameters.length - 1];
    let postOption = {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        uri: postUrl,
        resolveWithFullResponse: true,
        formData : {
            'url': pathParameters[pathParameters.length - 1]
        }
    };

    try {
        let postResponse = await rp(postOption);
    } catch (error) {
        return error.response.headers.location;
    }
}

async function getRedirectedUrl(toRedirect) {
    const options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        uri: toRedirect,
        resolveWithFullResponse: true,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'Accept': '*/*',
            'Cache-Control': 'no-cache'
        },
    };
    let response = await rp(options);
    return response.request.href;
}


exports.getDestinationUrl = getDestinationUrl;
