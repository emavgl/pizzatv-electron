var cloudscraper = require('cloudscraper');
var unpack = require('../unpacker.js');
var scraperutil = require('../scraperutil.js');

var exports = module.exports = {};

async function getParsedUrl(url) {
    let content = await cloudscraper.get(url);
    unpackScripts = unpack.findUnpackerScript(content);

    // iterate to find first mp4 link
    for (const unpackScript of unpackScripts) {
        try {
            let clearScript = unpack.unPack(unpackScript);
            if (clearScript.includes("Clappr")) {
                let videoUrls = scraperutil.findMultipleMatch(clearScript, /https:\/\/([^\s,<]+\.mp4)/g);
                videoUrls = videoUrls.filter(v => !v.includes('akvideo'));
                if (videoUrls) {
                    return videoUrls[0][0];
                }
            }
        } catch (error) {
            continue;
        }
    }

    return null;
};

exports.getParsedUrl = getParsedUrl;
