var unpack = require('../unpacker.js');
var scraperutil = require('../scraperutil.js');
var cloudscraper = require('cloudscraper');

var exports = module.exports = {};

async function getParsedUrl(url) {

    url = await transformUrl(url, 0);
    let content = await cloudscraper.get(url);
    unpackScripts = unpack.findUnpackerScript(content);

    // iterate to find first mp4 link
    for (const unpackScript of unpackScripts) {
        try {
            let clearScript = unpack.unPack(unpackScript);
            let videoUrls = scraperutil.findMultipleMatch(clearScript, /"(https:\/\/[^"]*v.mp4)"/g);
            if (videoUrls) {
                let finalUrl = videoUrls[0][1];
                let fakeUrls = scraperutil.findMultipleMatch(finalUrl, /\/3.\//g); 
                if (fakeUrls.length == 0) return finalUrl;
            }
        } catch (error) {
            continue;
        }
    }

    let videoUrls = scraperutil.findMultipleMatch(content, /"(https:\/\/[^"]*v.mp4)"/g);
    if (videoUrls && videoUrls.length > 0) return videoUrls[0][1];

    return null;
};

function getInputFormValues(htmlDoc) {
    const regex = /input type=.hidden.*id=.([a-z]+).*value=.([a-z0-9]+)./gm;
        
    // get all inputs
    let m;
    let formAttributes = {};
    while ((m = regex.exec(htmlDoc)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        formAttributes[m[1]] = m[2];
    }

    return formAttributes;
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

async function transformUrl(url, attempt) {

    if (attempt > 2) {
        console.log("Exceeded number of attemps");
        return url;
    }

    // cheerio loaded
    let content = await cloudscraper.get(url);
    let formAttributes = getInputFormValues(content);

    if (!isEmpty(formAttributes)) {
        let options = {
            uri: url,
            form: formAttributes,
            resolveWithFullResponse: true,
        };

        try {
            let res = await cloudscraper.post(options);
            if (res.body) {
                let content = res.body;
                let location = res.headers.location;
                if (content.includes("Continue") || (location && location.includes("file_code"))) {
                    if (location) {
                        return await transformUrl(location, attempt+1);
                    } else {
                        return await transformUrl(res.request.href, attempt+1);
                    }
                }
            }
            return res.request.href;    
        } catch (error) {
            console.log(error);
            return await transformUrl(url, attempt+1);
        }

    }

    return url;
}

exports.getParsedUrl = getParsedUrl;
