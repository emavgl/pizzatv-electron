var rp = require('request-promise-native');
var exports = module.exports = {};

async function getParsedUrl(url) {
    const options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        uri: url
      };

    const content = await rp(options);
    let reg  = /https?:\/\/speedvideo[^('+")]+mp4[^('+")]+/g;
    let results = [];
    let match = null;
    
    while ((match = reg.exec(content)) !== null) {
        results.push(match[0]);
    }

    return results[0];
};

exports.getParsedUrl = getParsedUrl;
