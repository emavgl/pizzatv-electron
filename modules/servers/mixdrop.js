var rp = require('request-promise-native');
var unpack = require('../unpacker.js');
var scraperutil = require('../scraperutil.js');

var exports = module.exports = {};

async function getParsedUrl(url) {
    const options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        uri: url
    };

    const content = await rp(options);
    unpackScripts = unpack.findUnpackerScript(content);

    // iterate to find first mp4 link
    for (const unpackScript of unpackScripts) {
        try {
            let clearScript = unpack.unPack(unpackScript);
            let videoUrl1 = scraperutil.findMultipleMatch(clearScript, /vsrc="(.*)";/g);
            let videoUrl2 = scraperutil.findMultipleMatch(clearScript, /vsr="(.*)";/g);
            let videoUrl = videoUrl1.length != 0 ? videoUrl1 : videoUrl2;
            if (videoUrl) {
                return 'https:' + videoUrl[0][1];
            }
        } catch (error) {
            continue;
        }
    }

    return null;
};

exports.getParsedUrl = getParsedUrl;
