var assert = require('chai').assert;
var mixdrop = require('../../modules/servers/mixdrop');

describe('mixdrop', function() {

  describe('vcrypt bypass', function() {
    it('it should bypass mixdrop', async () => {

      let testUrls = [
        "https://mixdrop.co/e/aw8lk34n",
      ]

      for (const url of testUrls) {
        let parsedUrl = await mixdrop.getParsedUrl(url);
        console.log(parsedUrl);
        assert.isString(parsedUrl, 'url has been parsed');
        assert.isNotEmpty(parsedUrl, 'url is not empty');
        assert.isTrue(parsedUrl.includes('.mp4'));
      }

    });
  });

});