var assert = require('chai').assert;
var wstream = require('../../modules/servers/wstream');

describe('wstream', function() {

  describe('wstream bypass', function() {
    it('it should bypass wstream', async () => {

      let testUrls = [
        "https://wstream.video/swembed/x21tbkziwl45.html",
        "https://wstream.video/api/vcmod/fastredirect/streaming.php?id=647691",
        "https://wstream.video/frlv56emi5id",
        "https://wstream.video/iss4ogtllemv",
        "https://wstream.video/kz2g1d81kwag",
        "https://wstream.video/ar2w4vci4akw",
        "https://wstream.video/us9phe3f8d10",
      ]

      for (const url of testUrls) {
        let parsedUrl = await wstream.getParsedUrl(url);
        console.log(parsedUrl);
        assert.isString(parsedUrl, 'url has been parsed');
        assert.isNotEmpty(parsedUrl, 'url is not empty');
        assert.isTrue(parsedUrl.includes('v.mp4'));
      }

    });
  });

});