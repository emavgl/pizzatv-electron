var assert = require('chai').assert;
var vcrypt = require('../../modules/servers/vcrypt');

describe('vcrypt', function() {

  describe('vcrypt bypass', function() {

    it('it should bypass vcrypt', async () => {

      let testUrls = [
        "https://vcrypt.net/wss/spgq0wurhhna",
        "https://vcrypt.net/shield/wB_sll_vCsIMhz3x9BZSdnWICa40nduMDT47kX7_ppl_md2lU1tN4Gh6OO473nuOsr_ppl_zG26D",
        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWOwnrQS78TKsMXMBi9sL3wfYATF1J_ppl_x5noeIue7_ppl_xmuQI",
        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWO8_ppl_ZhGrgqRq1tSN4GZh6mm5L1J6RIg1s6J5_ppl_lYUz1bbN",
        "https://vcrypt.net/wse/lpabekuulkdm",
         "https://vcrypt.net/wss/lpabekuulkdm",
        
        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWO0Jbq2sW0u_ppl_OGG5qsQ4FVvve11eygvm3ssYoo5_ppl_IxBtw",
        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWO5YD77vSnNYIMKSS0TYt9eUf_ppl_OnebgL_ppl_nF5BDe8MosTo",
        "https://vcrypt.net/shield/wB_sll_vCsIMhz3x9BZSdnWICeI8WXn_ppl__ppl_fhF7RXcAbISOkn0RmR8GEa1T0_ppl_CeaTd91o9",

        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWO6zMhZNDIUgYViXea5z7_ppl_KBhh8ctPSek9N_sll_R3VQ0SGBX",
        "https://vcrypt.net/shield/UFaOkCd4cvrIP769VqjWO_ppl_LJK4Ldg_sll_okyGNTHjJvjERGf5A8G03FHNvG_sll_yKbokgX",
        "https://vcrypt.net/shield/wB_sll_vCsIMhz3x9BZSdnWICYmm_ppl_5AjfJoBrH_ppl_ePclyLCiZy0Tn7uZ5yHuQoMYCn44A",
        "https://vcrypt.net/shield/wB_sll_vCsIMhz3x9BZSdnWICXP_ppl_efHY4pjAOGavydicVNYHDzA_sll_unQS_sll_xYffCPMXHNr",
      ]

      for (const url of testUrls) {
        let parsedUrl = await vcrypt.getDestinationUrl(url);
        console.log(parsedUrl);
        assert.isNotNull(parsedUrl);
        assert.isString(parsedUrl, 'url has been parsed');
        assert.isNotEmpty(parsedUrl, 'url is not empty');
        assert.isTrue(!parsedUrl.includes("vcrypt"));
        assert.isTrue(!parsedUrl.includes("4snip"));
      }

    });
    
  });

});