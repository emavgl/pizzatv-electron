var assert = require('chai').assert;
var akvideo = require('../../modules/servers/akvideo');

describe('akvideo', function() {

  describe('vcrypt bypass', function() {
    it('it should bypass akvideo', async () => {

      let testUrls = [
        "https://akvideo.stream/video/9ptevwl659dd",
        "https://akvideo.stream/emfm0un2rcqr",
        "https://akvideo.stream/t65awacgc0m9"
      ]

      for (const url of testUrls) {
        let parsedUrl = await akvideo.getParsedUrl(url);
        console.log(parsedUrl);
        assert.isString(parsedUrl, 'url has been parsed');
        assert.isNotEmpty(parsedUrl, 'url is not empty');
        assert.isTrue(parsedUrl.includes('v.mp4'));
      }

    });
  });

});